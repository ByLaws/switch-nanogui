/*
    The text box widget was contributed by Christian Schueller.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/**
 * \file nanogui/theme.h
 *
 * \brief Storage class for basic theme-related properties.
 */

#pragma once

#include <nanogui/common.h>
#include <nanogui/object.h>

NAMESPACE_BEGIN(nanogui)

/**
 * \class Theme theme.h nanogui/theme.h
 *
 * \brief Storage class for basic theme-related properties.
 */
class NANOGUI_EXPORT Theme : public Object {
public:
    Theme(NVGcontext *ctx);

    /* Fonts */
    /// The standard font face (default: ``"sans"`` from ``resources/roboto_regular.ttf``).
    int mFontNormal;
    /// The icon font face (default: ``"icons"`` from ``resources/entypo.ttf``).
    int mFontIcons;
    /**
     * The amount of scaling that is applied to each icon to fit the size of
     * NanoGUI widgets.  The default value is ``1.54f``, setting to e.g. higher
     * than ``2.0f`` is generally discouraged.
     */
    float mIconScale;

    /* Spacing-related parameters */
    // The overall scale (default: ``1``).
    float mScale;
    /// The font size for all widgets other than buttons and textboxes (default: `` 32``).
    int mStandardFontSize;
    /// The small font size (default: ``20``).
    int mSmallFontSize;
    /// The font size for text boxes (default: ``40``).
    int mTextBoxFontSize;
    /// Rounding radius for Window widget corners (default: ``4``).
    int mWindowCornerRadius;
    /// Default size of Window widget titles (default: ``60``).
    int mWindowHeaderHeight;
    /// Size of drop shadow rendered behind the Window widgets (default: ``20``).
    int mWindowDropShadowSize;
    /// Height of separator on top and bottom of buttons (default: ``2``).
    int mButtonSeparatorHeight;
    /// Padding from start of separator to start of text (default: ``4``).
    int mButtonTextLeftPadding;
    /// Rounding radius for Button (and derived types) widgets (default: ``4``).
    int mButtonCornerRadius;
    /// The padding to the left of a NavWidget (default: ``10``).
    int mNavLeftPadding;
    /// The padding to the right of a NavWidget (default: ``10``).
    int mNavRightPadding;
    /// The inner margin on a NavSidebar widget (default: ``10``).
    int mNavInnerMargin;
    /// The amount of horizontal padding for a NavSidebar widget (default: ``20``).
    int mNavButtonHorizontalPadding;
    /// The width of the selected indicator in a NavSidebar widget (default: ``5``).
    int mNavButtonIndicatorWidth;
    /// The amount of vertical padding for a NavSidebar widget (default: ``4``).
    int mNavButtonVerticalPadding;

    /* Generic colors */
    /**
     * The color of the drop shadow drawn behind widgets
     * (default: intensity=``0``, alpha=``128``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mDropShadow;
    /**
     * The transparency color
     * (default: intensity=``0``, alpha=``0``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mTransparent;
    /**
     * The dark border color
     * (default: intensity=``29``, alpha=``255``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mBorderDark;
    /**
     * The light border color
     * (default: intensity=``92``, alpha=``255``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mBorderLight;
    /**
     * The medium border color
     * (default: intensity=``35``, alpha=``255``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mBorderMedium;
    /**
     * The text color
     * (default: intensity=``255``, alpha=``160``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mTextColor;
    /**
     * The selected text color
     * (default: intensity=``255``, alpha=``80``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mSelectedTextColor;
    /**
     * The disable dtext color
     */
    Color mDisabledTextColor;
    /**
     * The text shadow color
     * (default: intensity=``0``, alpha=``160``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mTextColorShadow;
    /// The icon color (default: \ref nanogui::Theme::mTextColor).
    Color mIconColor;
    
    /**
     * The sidebar background color
     */
    Color mSidebarColor;

    /**
     * The main content color
     */
    Color mContentColor;

    /**
     * The item separator color
     */
    Color mSeparatorColor;

    /* Button colors */
    /**
     * The color the button changes to on press
     */
    Color mButtonPressedColor;

    /* Window colors */
    /**
     * The fill color for a Window that is not in focus
     * (default: intensity=``43``, alpha=``230``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mWindowFillUnfocused;
    /**
     * The fill color for a Window that is in focus
     * (default: intensity=``45``, alpha=``230``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mWindowFillFocused;
    /**
     * The title color for a Window that is not in focus
     * (default: intensity=``220``, alpha=``160``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mWindowTitleUnfocused;
    /**
     * The title color for a Window that is in focus
     * (default: intensity=``255``, alpha=``190``; see \ref nanogui::Color::Color(int,int)).
     */
    Color mWindowTitleFocused;

    /**
     * The top gradient color for Window headings
     * (default: \ref nanogui::Theme::mButtonGradientTopUnfocused).
     */
    Color mWindowHeaderGradientTop;
    /**
     * The bottom gradient color for Window headings
     * (default: \ref nanogui::Theme::mButtonGradientBotUnfocused).
     */
    Color mWindowHeaderGradientBot;
    /// The Window sidebar top separation color (default: \ref nanogui::Theme::mBorderLight).
    Color mWindowHeaderSepTop;
    /// The Window sidebar bottom separation color (default: \ref nanogui::Theme::mBorderDark).
    Color mWindowHeaderSepBot;

    /**
     * The popup window color
     * (default: intensity=``50``, alpha=``255``; see \ref nanogui::Color::Color(int,int))).
     */
    Color mWindowPopup;
    /**
     * The transparent popup window color
     * (default: intensity=``50``, alpha=``0``; see \ref nanogui::Color::Color(int,int))).
     */
    Color mWindowPopupTransparent;

    /// Icon to use for CheckBox widgets (default: ``ENTYPO_ICON_CHECK``).
    int mCheckBoxIcon;
    /// Icon to use for informational MessageDialog widgets (default: ``ENTYPO_ICON_INFO_WITH_CIRCLE``).
    int mMessageInformationIcon;
    /// Icon to use for interrogative MessageDialog widgets (default: ``ENTYPO_ICON_HELP_WITH_CIRCLE``).
    int mMessageQuestionIcon;
    /// Icon to use for warning MessageDialog widgets (default: ``ENTYPO_ICON_WARNING``).
    int mMessageWarningIcon;
    /// Icon to use on MessageDialog alt button (default: ``ENTYPO_ICON_CIRCLE_WITH_CROSS``).
    int mMessageAltButtonIcon;
    /// Icon to use on MessageDialog primary button (default: ``ENTYPO_ICON_CHECK``).
    int mMessagePrimaryButtonIcon;
    /// Icon to use for PopupButton widgets opening to the right (default: ``ENTYPO_ICON_CHEVRON_RIGHT``).
    int mPopupChevronRightIcon;
    /// Icon to use for PopupButton widgets opening to the left (default: ``ENTYPO_ICON_CHEVRON_LEFT``).
    int mPopupChevronLeftIcon;
    /// Icon to use when a TextBox has an up toggle (e.g. IntBox) (default: ``ENTYPO_ICON_CHEVRON_UP``).
    int mTextBoxUpIcon;
    /// Icon to use when a TextBox has a down toggle (e.g. IntBox) (default: ``ENTYPO_ICON_CHEVRON_DOWN``).
    int mTextBoxDownIcon;

protected:
    /// Default destructor does nothing; allows for inheritance.
    virtual ~Theme() { };

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

NAMESPACE_END(nanogui)
