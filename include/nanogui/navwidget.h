/*
    nanogui/navwidget.h -- A wrapper around the widgets NavSidebar and StackedWidget
    which hooks the two classes together.

    The nav widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/widget.h>
#include <functional>

NAMESPACE_BEGIN(nanogui)

/**
 * \class NavWidget navwidget.h nanogui/navwidget.h
 *
 * \brief A wrapper around the widgets NavSidebar and StackedWidget which hooks
 *        the two classes together.
 *
 * \rst
 *
 * .. warning::
 *
 *    Unlike other widgets, children may **not** be added *directly* to a
 *    NavWidget.  For example, the following code will raise an exception:
 *
 *    .. code-block:: cpp
 *
 *       // `this` might be say a nanogui::Screen instance
 *       Window *window = new Window(this, "Window Title");
 *       NavWidget *navWidget = window->add<NavWidget>();
 *       // this label would be a direct child of navWidget,
 *       // which is forbidden, so an exception will be raised
 *       new Label(navWidget, "Some Label");
 *
 *    Instead, you are expected to be creating navs and adding widgets to those.
 *
 *    .. code-block:: cpp
 *
 *       // `this` might be say a nanogui::Screen instance
 *       Window *window = new Window(this, "Window Title");
 *       NavWidget *navWidget = window->add<NavWidget>();
 *       // Create a nav first
 *       auto *layer = navWidget->createNav("Nav Name");
 *       // Add children to the created navs
 *       layer->setLayout(new GroupLayout());
 *       new Label(layer, "Some Label");
 *
 *    A slightly more involved example of creating a NavWidget can also be found
 *    in :ref:`nanogui_example_1` (search for ``navWidget`` in the file).
 *
 * \endrst
 */
class NANOGUI_EXPORT NavWidget : public Widget {
public:
    NavWidget(Widget *parent);

    /**
     * \brief Forcibly prevent mis-use of the class by throwing an exception.
     *        Children are not to be added directly to the NavWidget, see
     *        the class level documentation (\ref NavWidget) for an example.
     *
     * \throws std::runtime_error
     *     An exception is always thrown, as children are not allowed to be
     *     added directly to this Widget.
     */
    virtual void addChild(int index, Widget *widget) override;
    virtual bool gamepadAnalogEvent(int jid, int axis, float value);

    void setActiveNav(int navIndex);
    int activeNav() const;
    int navCount() const;

    /**
     * Sets the callable objects which is invoked when a nav is changed.
     * The argument provided to the callback is the index of the new active nav.
     */
    void setCallback(const std::function<void(int)> &callback) { mCallback = callback; };
    const std::function<void(int)> &callback() const { return mCallback; }

    /// Creates a new nav with the specified name and returns a pointer to the layer.
    Widget *createNav(const std::string &label);
    Widget *createNav(int index, const std::string &label);

    /// Inserts a nav at the end of the navs collection and associates it with the provided widget.
    void addNav(const std::string &label, Widget *nav);

    /// Inserts a nav into the navs collection at the specified index and associates it with the provided widget.
    void addNav(int index, const std::string &label, Widget *nav);

    /**
     * Removes the nav with the specified label and returns the index of the label.
     * Returns whether the removal was successful.
     */
    bool removeNav(const std::string &label);

    /// Removes the nav with the specified index.
    void removeNav(int index);

    /// Retrieves the label of the nav at a specific index.
    const std::string &navLabelAt(int index) const;

    /**
     * Retrieves the index of a specific nav using its nav label.
     * Returns -1 if there is no such nav.
     */
    int navLabelIndex(const std::string &label);

    /**
     * Retrieves the index of a specific nav using a widget pointer.
     * Returns -1 if there is no such nav.
     */
    int navIndex(Widget* nav);

    /**
     * This function can be invoked to ensure that the nav with the provided
     * index the is visible, i.e to track the given nav. Forwards to the nav
     * sidebar widget. This function should be used whenever the client wishes
     * to make the nav sidebar follow a newly added nav, as the content of the
     * new nav is made visible but the nav sidebar does not track it by default.
     */
    void ensureNavVisible(int index);

    /**
     * \brief Returns a ``const`` pointer to the Widget associated with the
     *        specified label.
     *
     * \param label
     *     The label used to create the nav.
     *
     * \return
     *     The Widget associated with this label, or ``nullptr`` if not found.
     */
    const Widget *nav(const std::string &label) const;

    /**
     * \brief Returns a pointer to the Widget associated with the specified label.
     *
     * \param label
     *     The label used to create the nav.
     *
     * \return
     *     The Widget associated with this label, or ``nullptr`` if not found.
     */
    Widget *nav(const std::string &label);

    /**
     * \brief Returns a ``const`` pointer to the Widget associated with the
     *        specified index.
     *
     * \param index
     *     The current index of the desired Widget.
     *
     * \return
     *     The Widget at the specified index, or ``nullptr`` if ``index`` is not
     *     a valid index.
     */
    const Widget *nav(int index) const;

    /**
     * \brief Returns a pointer to the Widget associated with the specified index.
     *
     * \param index
     *     The current index of the desired Widget.
     *
     * \return
     *     The Widget at the specified index, or ``nullptr`` if ``index`` is not
     *     a valid index.
     */
    Widget *nav(int index);

    virtual void performLayout(NVGcontext* ctx) override;
    virtual Vector2i preferredSize(NVGcontext* ctx) const override;
    virtual void draw(NVGcontext* ctx) override;

private:
    NavSidebar* mSidebar;
    StackedWidget* mContent;
    std::function<void(int)> mCallback;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

NAMESPACE_END(nanogui)
