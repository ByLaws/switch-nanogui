/*
    nanogui/navsidebar.h -- Widget used to control navs.

    The nav sidebar widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/widget.h>
#include <vector>
#include <string>
#include <functional>
#include <utility>
#include <iterator>

NAMESPACE_BEGIN(nanogui)

/**
 * \class NavSidebar navsidebar.h nanogui/navsidebar.h
 *
 * \brief A Nav navigable widget.
 */
class NANOGUI_EXPORT NavSidebar : public Widget {
public:
    NavSidebar(Widget *parent, const std::string &font = "sans");

    void setFont(const std::string& font) { mFont = font; }
    const std::string& font() const { return mFont; }
    bool overflowing() const { return mOverflowing; }

    /**
     * Sets the callable objects which is invoked when a nav button is pressed.
     * The argument provided to the callback is the index of the nav.
     */
    void setCallback(const std::function<void(int)>& callback) { mCallback = callback; };
    const std::function<void(int)>& callback() const { return mCallback; }

    void setActiveNav(int navIndex);
    int activeNav() const;
    bool isNavVisible(int index) const;
    int navCount() const { return (int) mNavButtons.size();  }

    /// Inserts a nav at the end of the navs collection.
    void addNav(const std::string& label);

    /// Inserts a nav into the navs collection at the specified index.
    void addNav(int index, const std::string& label);

    /**
     * Removes the nav with the specified label and returns the index of the label.
     * Returns -1 if there was no such nav
     */
    int removeNav(const std::string& label);

    /// Removes the nav with the specified index.
    void removeNav(int index);

    /// Retrieves the label of the nav at a specific index.
    const std::string& navLabelAt(int index) const;

    /**
     * Retrieves the index of a specific nav label.
     * Returns the number of navs (navsCount) if there is no such nav.
     */
    int navIndex(const std::string& label);

    /**
     * Recalculate the visible range of navs so that the nav with the specified
     * index is visible. The nav with the specified index will either be the
     * first or last visible one depending on the position relative to the
     * old visible range.
     */
    void ensureNavVisible(int index);

    /**
     * Returns a pair of Vectors describing the top left (pair.first) and the
     * bottom right (pair.second) positions of the rectangle containing the visible nav buttons.
     */
    std::pair<Vector2i, Vector2i> visibleButtonArea() const;

    virtual void performLayout(NVGcontext* ctx) override;
    virtual Vector2i preferredSize(NVGcontext* ctx) const override;
    virtual bool mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers) override;
    virtual bool gamepadAnalogEvent(int jid, int axis, float value);

    virtual void draw(NVGcontext* ctx) override;

private:
    /**
     * \class NavButton navsidebar.h
     *
     * \brief Implementation class of the actual nav buttons.
     */
    class NavButton {
    public:
        constexpr static const char* dots = "...";

        NavButton(NavSidebar& sidebar, const std::string& label);

        void setLabel(const std::string& label) { mLabel = label; }
        const std::string& label() const { return mLabel; }
        void setSize(const Vector2i& size) { mSize = size; }
        const Vector2i& size() const { return mSize; }

        Vector2i preferredSize(NVGcontext* ctx) const;
        void calculateVisibleString(NVGcontext* ctx);
        void drawAtPosition(NVGcontext* ctx, const Vector2i& position, const Vector2i& size, bool active, bool hovered, bool selected);

    private:
        NavSidebar* mSidebar;
        std::string mLabel;
        Vector2i mSize;

        /**
         * \struct StringView navsidebar.h nanogui/navsidebar.h
         *
         * \brief Helper struct to represent the NavButton.
         */
        struct StringView {
            const char* first = nullptr;
            const char* last = nullptr;
        };
        StringView mVisibleText;
        int mVisibleWidth = 0;
    };

    using NavIterator = std::vector<NavButton>::iterator;
    using ConstNavIterator = std::vector<NavButton>::const_iterator;

    /// The location in which the Widget will be facing.
    enum class ClickLocation {
        LeftControls, RightControls, NavButtons
    };

    NavIterator visibleBegin() { return std::next(mNavButtons.begin(), mVisibleStart); }
    NavIterator visibleEnd() { return std::next(mNavButtons.begin(), mVisibleEnd); }
    NavIterator activeIterator() { return std::next(mNavButtons.begin(), mActiveNav); }
    NavIterator navIterator(int index) { return std::next(mNavButtons.begin(), index); }

    ConstNavIterator visibleBegin() const { return std::next(mNavButtons.begin(), mVisibleStart); }
    ConstNavIterator visibleEnd() const { return std::next(mNavButtons.begin(), mVisibleEnd); }
    ConstNavIterator activeIterator() const { return std::next(mNavButtons.begin(), mActiveNav); }
    ConstNavIterator navIterator(int index) const { return std::next(mNavButtons.begin(), index); }

    /// Given the beginning of the visible navs, calculate the end.
    void calculateVisibleEnd();

    void drawControls(NVGcontext* ctx);

    std::function<void(int)> mCallback;
    std::vector<NavButton> mNavButtons;
    int mHoveredButton = -1;
    int mSelectedButton = -1;
    int mVisibleStart = 0;
    int mVisibleEnd = 0;
    int mActiveNav = 0;
    bool mOverflowing = false;

    std::string mFont;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

NAMESPACE_END(nanogui)
