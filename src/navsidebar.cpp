/*
    nanogui/navsidebar.cpp -- Widget used to control navs.

    The nav sidebar widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/navsidebar.h>
#include <nanogui/theme.h>
#include <nanogui/opengl.h>
#include <numeric>
#include <iostream>

NAMESPACE_BEGIN(nanogui)

NavSidebar::NavButton::NavButton(NavSidebar &sidebar, const std::string &label)
    : mSidebar(&sidebar), mLabel(label) { }

Vector2i NavSidebar::NavButton::preferredSize(NVGcontext *ctx) const {
    // No need to call nvg font related functions since this is done by the nav sidebar implementation
    float bounds[4];
    int labelWidth = nvgTextBounds(ctx, 0, 0, mLabel.c_str(), nullptr, bounds);
    int buttonWidth = labelWidth + 2 * mSidebar->theme()->mNavButtonHorizontalPadding;
    int buttonHeight = bounds[3] - bounds[1] + 2 * mSidebar->theme()->mNavButtonVerticalPadding;
    return Vector2i(buttonWidth, buttonHeight);
}

void NavSidebar::NavButton::calculateVisibleString(NVGcontext *ctx) {
    // The size must have been set in by the enclosing nav sidebar.
    NVGtextRow displayedText;
    nvgTextBreakLines(ctx, mLabel.c_str(), nullptr, mSize.x(), &displayedText, 1);

    // Check to see if the text need to be truncated.
    if (displayedText.next[0]) {
        auto truncatedWidth = nvgTextBounds(ctx, 0.0f, 0.0f,
                                            displayedText.start, displayedText.end, nullptr);
        auto dotsWidth = nvgTextBounds(ctx, 0.0f, 0.0f, dots, nullptr, nullptr);
        while ((truncatedWidth + dotsWidth + mSidebar->theme()->mNavButtonHorizontalPadding) > mSize.x()
                && displayedText.end != displayedText.start) {
            --displayedText.end;
            truncatedWidth = nvgTextBounds(ctx, 0.0f, 0.0f,
                                           displayedText.start, displayedText.end, nullptr);
        }

        // Remember the truncated width to know where to display the dots.
        mVisibleWidth = truncatedWidth;
        mVisibleText.last = displayedText.end;
    } else {
        mVisibleText.last = nullptr;
        mVisibleWidth = 0;
    }
    mVisibleText.first = displayedText.start;
}

void NavSidebar::NavButton::drawAtPosition(NVGcontext *ctx, const Vector2i& position, const Vector2i& size, bool active, bool hovered, bool selected) {
    int xPos = position.x();
    int yPos = position.y();
    int width = size.x();
    int height = mSize.y();
    auto theme = mSidebar->theme();

    nvgBeginPath(ctx);
    nvgRect(ctx, xPos, yPos, width, height); 
    
    nvgFillColor(ctx, theme->mSidebarColor);
    
    if (hovered)
        nvgFillColor(ctx, theme->mButtonPressedColor);
    else
        nvgFillColor(ctx, theme->mSidebarColor);

    nvgFill(ctx);

    if (active) {
        nvgBeginPath(ctx);
        nvgRect(ctx, xPos, yPos, theme->mNavButtonIndicatorWidth, height); 
        nvgFillColor(ctx, theme->mSelectedTextColor);
        nvgFill(ctx);
    }
    
    // Draw the text with some padding
    int textX = xPos + theme->mNavButtonHorizontalPadding;
    int textY = yPos + theme->mNavButtonVerticalPadding;
    NVGcolor textColor;
    if (active)
        textColor = theme->mSelectedTextColor;
    else
        textColor = theme->mTextColor;
    
    nvgBeginPath(ctx);
    nvgFillColor(ctx, textColor);
    nvgText(ctx, textX, textY, mVisibleText.first, mVisibleText.last);
    if (mVisibleText.last != nullptr)
        nvgText(ctx, textX + mVisibleWidth, textY, dots, nullptr);

    if (selected) {
        nvgBeginPath(ctx);
        nvgRect(ctx, xPos - 0.5f, yPos - 0.5f, width + 1, height + 1);
        nvgStrokeWidth(ctx, 10.0f);
        nvgStrokeColor(ctx, nvgRGBA(255, 0, 0, 255));
        nvgStroke(ctx);
    }
}

NavSidebar::NavSidebar(Widget* parent, const std::string& font)
    : Widget(parent), mFont(font) { mSelectable = false;}

void NavSidebar::setActiveNav(int navIndex) {
    assert(navIndex < navCount());
    mActiveNav = navIndex;
    if (mCallback)
        mCallback(navIndex);
}

int NavSidebar::activeNav() const {
    return mActiveNav;
}

bool NavSidebar::isNavVisible(int index) const {
    return index >= mVisibleStart && index < mVisibleEnd;
}

void NavSidebar::addNav(const std::string & label) {
    addNav(navCount(), label);
}

void NavSidebar::addNav(int index, const std::string &label) {
    assert(index <= navCount());
    mNavButtons.insert(std::next(mNavButtons.begin(), index), NavButton(*this, label));
    setActiveNav(index);
}

int NavSidebar::removeNav(const std::string &label) {
    auto element = std::find_if(mNavButtons.begin(), mNavButtons.end(),
                                [&](const NavButton& tb) { return label == tb.label(); });
    int index = (int) std::distance(mNavButtons.begin(), element);
    if (element == mNavButtons.end())
        return -1;
    mNavButtons.erase(element);
    if (index == mActiveNav && index != 0)
        setActiveNav(index - 1);
    return index;
}

void NavSidebar::removeNav(int index) {
    assert(index < navCount());
    mNavButtons.erase(std::next(mNavButtons.begin(), index));
    if (index == mActiveNav && index != 0)
        setActiveNav(index - 1);
}

const std::string& NavSidebar::navLabelAt(int index) const {
    assert(index < navCount());
    return mNavButtons[index].label();
}

int NavSidebar::navIndex(const std::string &label) {
    auto it = std::find_if(mNavButtons.begin(), mNavButtons.end(),
                           [&](const NavButton& tb) { return label == tb.label(); });
    if (it == mNavButtons.end())
        return -1;
    return (int) (it - mNavButtons.begin());
}

void NavSidebar::ensureNavVisible(int index) {
    auto visibleArea = visibleButtonArea();
    auto visibleWidth = visibleArea.second.x() - visibleArea.first.x();
    int allowedVisibleWidth = mSize.x();
    assert(allowedVisibleWidth >= visibleWidth);
    assert(index >= 0 && index < (int) mNavButtons.size());

    auto first = visibleBegin();
    auto last = visibleEnd();
    auto goal = navIterator(index);

    // Reach the goal nav with the visible range.
    if (goal < first) {
        do {
            --first;
            visibleWidth += first->size().x();
        } while (goal < first);
        while (allowedVisibleWidth < visibleWidth) {
            --last;
            visibleWidth -= last->size().x();
        }
    }
    else if (goal >= last) {
        do {
            visibleWidth += last->size().x();
            ++last;
        } while (goal >= last);
        while (allowedVisibleWidth < visibleWidth) {
            visibleWidth -= first->size().x();
            ++first;
        }
    }

    // Check if it is possible to expand the visible range on either side.
    while (first != mNavButtons.begin()
           && std::next(first, -1)->size().x() < allowedVisibleWidth - visibleWidth) {
        --first;
        visibleWidth += first->size().x();
    }
    while (last != mNavButtons.end()
           && last->size().x() < allowedVisibleWidth - visibleWidth) {
        visibleWidth += last->size().x();
        ++last;
    }

    mVisibleStart = (int) std::distance(mNavButtons.begin(), first);
    mVisibleEnd = (int) std::distance(mNavButtons.begin(), last);
}

std::pair<Vector2i, Vector2i> NavSidebar::visibleButtonArea() const {
    if (mVisibleStart == mVisibleEnd)
        return { Vector2i::Zero(), Vector2i::Zero() };
    auto topLeft = mPos;
    auto width = std::accumulate(visibleBegin(), visibleEnd(), 0,
                                 [](int acc, const NavButton& tb) {
        return acc + tb.size().x();
    });
    auto bottomRight = mPos + Vector2i(width, mSize.y());
    return { topLeft, bottomRight };
}

void NavSidebar::performLayout(NVGcontext* ctx) {
    Widget::performLayout(ctx);

    Vector2i currentPosition = Vector2i::Zero();
    // Place the nav buttons relative to the beginning of the nav sidebar.
    for (auto& nav : mNavButtons) {
        auto navPreferred = nav.preferredSize(ctx);
        nav.setSize(navPreferred);
        nav.calculateVisibleString(ctx);
        currentPosition.y() += navPreferred.y();
    }
    calculateVisibleEnd();
}

Vector2i NavSidebar::preferredSize(NVGcontext* ctx) const {
    // Set up the nvg context for measuring the text inside the nav buttons.
    nvgFontFace(ctx, mFont.c_str());
    nvgFontSize(ctx, fontSize());
    nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
    Vector2i size = Vector2i(0, 0);
    for (auto& nav : mNavButtons) {
        auto navPreferred = nav.preferredSize(ctx);
        size.x() = std::max(size.x(), navPreferred.x());
        size.y() += navPreferred.y();
    }
    return size;
}

bool NavSidebar::gamepadAnalogEvent(int jid, int axis, float value) {
    if (mSelected && (axis == GLFW_GAMEPAD_AXIS_LEFT_Y || axis == GLFW_GAMEPAD_AXIS_RIGHT_Y)) { // Advanced grid layout doesnt work atm
        if (mSelectedButton == -1)
            mSelectedButton = 0;
        if (value < -0.7f) {
            if (mSelectedButton > 0)
                mSelectedButton--;
            return true;
        } else if (value > 0.7f) {
            if ((size_t)mSelectedButton + 1< mNavButtons.size())
                mSelectedButton++;
            return true;
        }
    } else if (!mSelected) {
        mSelectedButton = -1;
    }
    return false;
}

bool NavSidebar::mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers) {
    Widget::mouseButtonEvent(p, button, down, modifiers);
    if (button == GLFW_MOUSE_BUTTON_1) {
        auto first = visibleBegin();
        auto last = visibleEnd();
        int currentPosition = mPos.y();
        int endPosition = p.y();
        auto firstInvisible = std::find_if(first, last,
                                           [&currentPosition, endPosition](const NavButton& tb) {
            currentPosition += tb.size().y();
            return currentPosition > endPosition;
        });

        // Did not click on any of the nav buttons
        if (firstInvisible == last) {
            if (mSelected) {
                if (down) {
                    mHoveredButton = mSelectedButton;
                    return true;
                } else {
                    setActiveNav(mSelectedButton);
                }
            }
            mHoveredButton = -1;
            return true;
        }

        if (down) {
            mHoveredButton = (int) std::distance(mNavButtons.begin(), firstInvisible);
        } else {
            // Update the active nav, reset hovered button and invoke the callback.
            mHoveredButton = -1;
            setActiveNav((int) std::distance(mNavButtons.begin(), firstInvisible));
        }
        return true;
    }
    return false;
}

void NavSidebar::draw(NVGcontext* ctx) {
    // Draw controls.
    Widget::draw(ctx);

    // Set up common text drawing settings.
    nvgFontFace(ctx, mFont.c_str());
    nvgFontSize(ctx, fontSize());
    nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);

    auto current = visibleBegin();
    auto last = visibleEnd();
    auto active = std::next(mNavButtons.begin(), mActiveNav);
    auto selected = std::next(mNavButtons.begin(), mSelectedButton);

    Vector2i currentPosition = mPos;

    // Flag to draw the active nav last. Looks a little bit better.
    bool drawActive = false;
    Vector2i activePosition = Vector2i::Zero();

    // Draw inactive visible buttons.
    while (current != last) {
        if (current == active) {
            drawActive = true;
            activePosition = currentPosition;
        } else {
            bool btnSelected = false;
            if (mSelectedButton != -1)
                btnSelected = current == selected;
            if (mHoveredButton != -1) {
                auto hovered = std::next(mNavButtons.begin(), mHoveredButton);
                current->drawAtPosition(ctx, currentPosition, preferredSize(ctx), false, current == hovered, btnSelected);
            } else {
                current->drawAtPosition(ctx, currentPosition, preferredSize(ctx), false, false, btnSelected);
            }
        }
        currentPosition.y() += current->size().y();
        ++current;
    }

    // Draw active visible button.
    if (drawActive)
        active->drawAtPosition(ctx, activePosition, preferredSize(ctx), true, false, active == selected);
}

void NavSidebar::calculateVisibleEnd() {
    auto first = visibleBegin();
    auto last = mNavButtons.end();
    int currentPosition = 0;
    int lastPosition = mSize.x();
    auto firstInvisible = std::find_if(first, last,
                                       [&currentPosition, lastPosition](const NavButton& tb) {
        currentPosition += tb.size().x();
        return currentPosition > lastPosition;
    });
    mVisibleEnd = (int) std::distance(mNavButtons.begin(), firstInvisible);
}

NAMESPACE_END(nanogui)
