/*
    nanogui/navwidget.cpp -- A wrapper around the widgets NavSidebar and StackedWidget
    which hooks the two classes together.

    The nav widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/navwidget.h>
#include <nanogui/navsidebar.h>
#include <nanogui/stackedwidget.h>
#include <nanogui/theme.h>
#include <nanogui/opengl.h>
#include <nanogui/window.h>
#include <nanogui/screen.h>
#include <algorithm>

NAMESPACE_BEGIN(nanogui)

NavWidget::NavWidget(Widget* parent)
    : Widget(parent)
    , mSidebar(new NavSidebar(nullptr)) // create using nullptr, add children below
    , mContent(new StackedWidget(nullptr)) {

    // since NavWidget::addChild is going to throw an exception to prevent
    // mis-use of this class, add the child directly
    Widget::addChild(childCount(), mSidebar);
    Widget::addChild(childCount(), mContent);

    mSidebar->setCallback([this](int i) {
        mContent->setSelectedIndex(i);
        if (mCallback)
            mCallback(i);
    });
}

void NavWidget::addChild(int /*index*/, Widget * /*widget*/) {
    // there may only be two children: mSidebar and mContent, created in the constructor
    throw std::runtime_error(
        "NavWidget: do not add children directly to the NavWidget, create navs "
        "and add children to the navs.  See NavWidget class documentation for "
        "example usage."
    );
}

void NavWidget::setActiveNav(int navIndex) {
    mSidebar->setActiveNav(navIndex);
    mContent->setSelectedIndex(navIndex);
}

int NavWidget::activeNav() const {
    assert(mSidebar->activeNav() == mContent->selectedIndex());
    return mContent->selectedIndex();
}

int NavWidget::navCount() const {
    assert(mContent->childCount() == mSidebar->navCount());
    return mSidebar->navCount();
}

Widget* NavWidget::createNav(int index, const std::string &label) {
    Widget* nav = new Widget(nullptr);
    addNav(index, label, nav);
    return nav;
}

Widget* NavWidget::createNav(const std::string &label) {
    return createNav(navCount(), label);
}

void NavWidget::addNav(const std::string &name, Widget *nav) {
    addNav(navCount(), name, nav);
}

void NavWidget::addNav(int index, const std::string &label, Widget *nav) {
    assert(index <= navCount());
    // It is important to add the content first since the callback
    // of the sidebar will automatically fire when a new nav is added.
    mContent->addChild(index, nav);
    mSidebar->addNav(index, label);
    assert(mSidebar->navCount() == mContent->childCount());
}

int NavWidget::navLabelIndex(const std::string &label) {
    return mSidebar->navIndex(label);
}

int NavWidget::navIndex(Widget* nav) {
    return mContent->childIndex(nav);
}

void NavWidget::ensureNavVisible(int index) {
    if (!mSidebar->isNavVisible(index))
        mSidebar->ensureNavVisible(index);
}

const Widget *NavWidget::nav(const std::string &navName) const {
    int index = mSidebar->navIndex(navName);
    if (index == -1 || index == mContent->childCount())
        return nullptr;
    return mContent->children()[index];
}

Widget *NavWidget::nav(const std::string &navName) {
    int index = mSidebar->navIndex(navName);
    if (index == -1 || index == mContent->childCount())
        return nullptr;
    return mContent->children()[index];
}

const Widget *NavWidget::nav(int index) const {
    if (index < 0 || index >= mContent->childCount())
        return nullptr;
    return mContent->children()[index];
}

Widget *NavWidget::nav(int index) {
    if (index < 0 || index >= mContent->childCount())
        return nullptr;
    return mContent->children()[index];
}

bool NavWidget::removeNav(const std::string &navName) {
    int index = mSidebar->removeNav(navName);
    if (index == -1)
        return false;
    mContent->removeChild(index);
    return true;
}

void NavWidget::removeNav(int index) {
    assert(mContent->childCount() < index);
    mSidebar->removeNav(index);
    mContent->removeChild(index);
    if (activeNav() == index)
        setActiveNav(index == (index - 1) ? index - 1 : 0);
}

const std::string &NavWidget::navLabelAt(int index) const {
    return mSidebar->navLabelAt(index);
}

bool NavWidget::gamepadAnalogEvent(int jid, int axis, float value) {
    bool ret = false;

    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible()) {
            if (child->gamepadAnalogEvent(jid, axis, value))
                ret = true;
        }
    }
    if (ret)
        return ret;

    if (axis == GLFW_GAMEPAD_AXIS_LEFT_X || axis == GLFW_GAMEPAD_AXIS_RIGHT_X) { // Advanced grid layout doesnt work atm
        if (value > 0.7f) {
            mContent->setSelected(true);
            mContent->childAt(mContent->selectedIndex())->setSelected(true);
            mSidebar->setSelectedRecursive(false);
            return true;
        } else if (value < -0.7f) {
            mSidebar->setSelected(true);
            mContent->setSelectedRecursive(false);
            return true;
        }
    }
    return false;
}

void NavWidget::performLayout(NVGcontext* ctx) {
    int sidebarWidth = mSidebar->preferredSize(ctx).x();
    int margin = mTheme->mNavInnerMargin;
    mSidebar->setPosition({ mTheme->mNavLeftPadding, mTheme->mNavRightPadding});
    mSidebar->setSize({ mSize.x(), sidebarWidth });
    mSidebar->performLayout(ctx);
    mContent->setPosition({ sidebarWidth + margin + mTheme->mNavLeftPadding + mTheme->mNavRightPadding, margin });
    mContent->setSize({ mSize.x() - 2 * margin - sidebarWidth - mTheme->mNavLeftPadding - mTheme->mNavRightPadding, mSize.y() - 2*margin});
    mContent->performLayout(ctx);
}

Vector2i NavWidget::preferredSize(NVGcontext* ctx) const {
    auto contentSize = mContent->preferredSize(ctx);
    auto sidebarSize = mSidebar->preferredSize(ctx);
    int margin = mTheme->mNavInnerMargin;
    auto borderSize = Vector2i(mTheme->mNavLeftPadding + mTheme->mNavRightPadding + 2 * margin, 2 * margin);
    Vector2i navPreferredSize = contentSize + borderSize + Vector2i(0, sidebarSize.x());
    return navPreferredSize;
}

void NavWidget::draw(NVGcontext* ctx) {
    int navWidth = mSidebar->preferredSize(ctx).x();
    
    nvgSave(ctx);
    nvgBeginPath(ctx);
    nvgRect(ctx, mPos.x(), mPos.y(), navWidth + mTheme->mNavLeftPadding + mTheme->mNavRightPadding, mSize.y()); 
    nvgFillColor(ctx, mTheme->mSidebarColor);
    nvgFill(ctx);
    nvgRestore(ctx);
    Widget::draw(ctx);
}

NAMESPACE_END(nanogui)
