/*
    src/widget.cpp -- Base class of all widgets

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/widget.h>
#include <nanogui/layout.h>
#include <nanogui/theme.h>
#include <nanogui/window.h>
#include <nanogui/opengl.h>
#include <nanogui/screen.h>
#include <nanogui/serializer/core.h>
#include <iostream>
#include <cmath>
NAMESPACE_BEGIN(nanogui)

Widget::Widget(Widget *parent)
    : mParent(nullptr), mTheme(nullptr), mLayout(nullptr),
      mPos(Vector2i::Zero()), mSize(Vector2i::Zero()),
      mFixedSize(Vector2i::Zero()), mVisible(true), mEnabled(true),
      mFocused(false), mMouseFocus(false), mSelected(false), mSelectable(true), mTooltip(""), mFontSize(-1.0f),
      mIconExtraScale(1.0f), mCursor(Cursor::Arrow) {
    if (parent)
        parent->addChild(this);
}

Widget::~Widget() {
    for (auto child : mChildren) {
        if (child)
            child->decRef();
    }
}

void Widget::setTheme(Theme *theme) {
    if (mTheme.get() == theme)
        return;
    mTheme = theme;
    for (auto child : mChildren)
        child->setTheme(theme);
}

int Widget::fontSize() const {
    return (mFontSize < 0 && mTheme) ? mTheme->mStandardFontSize : mFontSize;
}

Vector2i Widget::preferredSize(NVGcontext *ctx) const {
    if (mLayout)
        return mLayout->preferredSize(ctx, this);
    else
        return mSize;
}

void Widget::performLayout(NVGcontext *ctx) {
    if (mLayout) {
        mLayout->performLayout(ctx, this);
    } else {
        for (auto c : mChildren) {
            Vector2i pref = c->preferredSize(ctx), fix = c->fixedSize();
            c->setSize(Vector2i(
                fix[0] ? fix[0] : pref[0],
                fix[1] ? fix[1] : pref[1]
            ));
            c->performLayout(ctx);
        }
    }
}

Widget *Widget::findWidget(const Vector2i &p) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible() && child->contains(p - mPos))
            return child->findWidget(p - mPos);
    }
    return contains(p) ? this : nullptr;
}

void Widget::setSelectedRecursive(bool selected) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        child->setSelectedRecursive(selected);
    }
    mSelected = selected;
}

// Figure out sublayouts
bool Widget::gamepadButtonEvent(int jid, int button, int action) {
    bool last = false;
    
    if (mChildren.size())
        last = true;

    for (size_t i = 0; i < mChildren.size(); i++) {
        last = !last | !mChildren[i]->selected();
        if (mChildren[i]->gamepadButtonEvent(jid, button, action))
            return true;
    }

    if (mSelected && !last) {
        if (button == GLFW_GAMEPAD_BUTTON_A) {
            mouseButtonEvent(mPos + mSize / 2, GLFW_MOUSE_BUTTON_1, action, 0);
            return true;
        }
    }

    return false;
}
// Figure out sublayouts
bool Widget::gamepadAnalogEvent(int jid, int axis, float value) {
    bool ret = false;

    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible()) {
            if (child->gamepadAnalogEvent(jid, axis, value))
                ret = true;
        }
    }
    if (ret)
        return ret;

    if (mLayout && mSelected && (axis != GLFW_GAMEPAD_AXIS_LEFT_TRIGGER ||
             axis != GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER)) { // Advanced grid layout doesnt work atm
        int* cols = mLayout->colCount();
        int rows = mLayout->rowCount();
        int index = 0;
        bool left = false, right = false, up = false, down = false;

        if (axis == GLFW_GAMEPAD_AXIS_LEFT_X || axis == GLFW_GAMEPAD_AXIS_RIGHT_X) {
            if (value > 0.7f)
                right = true;
            else if (value < -0.7f)
                left = true;
        } else if (axis == GLFW_GAMEPAD_AXIS_LEFT_Y || axis == GLFW_GAMEPAD_AXIS_RIGHT_Y) {
            if (value > 0.7f)
                down = true;
            else if (value < -0.7f)
                up = true;
        }
        while (1) {
            for (int i = 1; i <= rows; i++) {
                for (int j = 0; j < cols[i]; j++) {
                    std::cout << "row " << i << " col " << j << " rows " << rows << " cols (for current row)" << cols[i] << " childsize:" << mChildren.size() << std::endl;
                    std::cout << "index: " << index << std::endl;
                    if (mChildren[index + j]->selected()) {
                        index += j;

                        std::cout << "found with j: " << j << std::endl;
                        std::cout << "found index: " << index << std::endl;
                        if (right && j + 1 < cols[i]) {
                            std::cout << "right" << std::endl;
                            mChildren[index]->setSelectedRecursive(false);
                            index += 1;
                        } else if (left && j > 0) {
                            std::cout << "left" << std::endl;
                            mChildren[index]->setSelectedRecursive(false);
                            index -= 1;
                        } else if (down && i < rows) {
                            float position = ((float)j / (cols[i] + 1)) * ((float)cols[i+1] + 1.0f);
                            std::cout << "down" << std::endl;
                            mChildren[index]->setSelectedRecursive(false);
                            index += (cols[i] - j) + round(position);
                        } else if (up && i > 1) {
                            float position = ((float)j / (cols[i] + 1)) * ((float)cols[i-1] + 1.0f);
                            std::cout << "up" << std::endl;
                            mChildren[index]->setSelectedRecursive(false);
                            std::cout << "upS" << std::endl;
                            index -= (cols[i-1] - round(position) - j);
                        } else {
                            return false;
                        }
                        std::cout << "try with index: " << index << std::endl;
                        mChildren[index]->setSelected(true);
                        return true;
                    
                    }
                }
                index += cols[i];
            }
            mChildren[0]->setSelected(true);
            index = 0;
        }
    }
    return false;
}

bool Widget::mouseButtonEvent(const Vector2i &p, int button, bool down, int modifiers) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (child->visible() && child->contains(p - mPos) &&
            child->mouseButtonEvent(p - mPos, button, down, modifiers))
            return true;
    }
    if (button == GLFW_MOUSE_BUTTON_1 && !down && !mFocused)
        requestFocus();
    return false;
}

bool Widget::mouseMotionEvent(const Vector2i &p, const Vector2i &rel, int button, int modifiers) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (!child->visible())
            continue;
        bool contained = child->contains(p - mPos), prevContained = child->contains(p - mPos - rel);
        if (contained != prevContained)
            child->mouseEnterEvent(p, contained);
        if ((contained || prevContained) &&
            child->mouseMotionEvent(p - mPos, rel, button, modifiers))
            return true;
    }
    return false;
}

bool Widget::scrollEvent(const Vector2i &p, const Vector2f &rel) {
    for (auto it = mChildren.rbegin(); it != mChildren.rend(); ++it) {
        Widget *child = *it;
        if (!child->visible())
            continue;
        if (child->contains(p - mPos) && child->scrollEvent(p - mPos, rel))
            return true;
    }
    return false;
}

bool Widget::mouseDragEvent(const Vector2i &, const Vector2i &, int, int) {
    return false;
}

bool Widget::mouseEnterEvent(const Vector2i &, bool enter) {
    mMouseFocus = enter;
    return false;
}

bool Widget::focusEvent(bool focused) {
    mFocused = focused;
    return false;
}

bool Widget::keyboardEvent(int, int, int, int) {
    return false;
}

bool Widget::keyboardCharacterEvent(unsigned int) {
    return false;
}

void Widget::addChild(int index, Widget * widget) {
    assert(index <= childCount());
    mChildren.insert(mChildren.begin() + index, widget);
    widget->incRef();
    widget->setParent(this);
    widget->setTheme(mTheme);
}

void Widget::addChild(Widget * widget) {
    addChild(childCount(), widget);
}

void Widget::removeChild(const Widget *widget) {
    mChildren.erase(std::remove(mChildren.begin(), mChildren.end(), widget), mChildren.end());
    widget->decRef();
}

void Widget::removeChild(int index) {
    Widget *widget = mChildren[index];
    mChildren.erase(mChildren.begin() + index);
    widget->decRef();
}

int Widget::childIndex(Widget *widget) const {
    auto it = std::find(mChildren.begin(), mChildren.end(), widget);
    if (it == mChildren.end())
        return -1;
    return (int) (it - mChildren.begin());
}

Window *Widget::window() {
    Widget *widget = this;
    while (true) {
        if (!widget)
            throw std::runtime_error(
                "Widget:internal error (could not find parent window)");
        Window *window = dynamic_cast<Window *>(widget);
        if (window)
            return window;
        widget = widget->parent();
    }
}

Screen *Widget::screen() {
    Widget *widget = this;
    while (true) {
        if (!widget)
            throw std::runtime_error(
                "Widget:internal error (could not find parent screen)");
        Screen *screen = dynamic_cast<Screen *>(widget);
        if (screen)
            return screen;
        widget = widget->parent();
    }
}

void Widget::requestFocus() {
    Widget *widget = this;
    while (widget->parent())
        widget = widget->parent();
    ((Screen *) widget)->updateFocus(this);
}

void Widget::draw(NVGcontext *ctx) {
    if (mSelected && mSelectable && !mLayout) {
    nvgBeginPath(ctx);
    nvgRect(ctx, mPos.x() - 0.5f, mPos.y() - 0.5f, mSize.x() + 1, mSize.y() + 1);
    nvgStrokeWidth(ctx, 10.0f);
    nvgStrokeColor(ctx, nvgRGBA(255, 0, 0, 255));
    nvgStroke(ctx);
    }
    if (mChildren.empty())
        return;

    nvgSave(ctx);
    nvgTranslate(ctx, mPos.x(), mPos.y());
    for (auto child : mChildren) {
        if (child->visible()) {
            nvgSave(ctx);
            nvgIntersectScissor(ctx, child->mPos.x(), child->mPos.y(), child->mSize.x(), child->mSize.y());
            child->draw(ctx);
            nvgRestore(ctx);
        }
    }
    nvgRestore(ctx);
}

void Widget::save(Serializer &s) const {
    s.set("position", mPos);
    s.set("size", mSize);
    s.set("fixedSize", mFixedSize);
    s.set("visible", mVisible);
    s.set("enabled", mEnabled);
    s.set("focused", mFocused);
    s.set("tooltip", mTooltip);
    s.set("fontSize", mFontSize);
    s.set("cursor", (int) mCursor);
}

bool Widget::load(Serializer &s) {
    if (!s.get("position", mPos)) return false;
    if (!s.get("size", mSize)) return false;
    if (!s.get("fixedSize", mFixedSize)) return false;
    if (!s.get("visible", mVisible)) return false;
    if (!s.get("enabled", mEnabled)) return false;
    if (!s.get("focused", mFocused)) return false;
    if (!s.get("tooltip", mTooltip)) return false;
    if (!s.get("fontSize", mFontSize)) return false;
    if (!s.get("cursor", mCursor)) return false;
    return true;
}

NAMESPACE_END(nanogui)
